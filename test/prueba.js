
let Asignatura = artifacts.require("Asignatura");


contract('Asignatura:', accounts => {

    let asignatura;

    before(async () => {
        asignatura = await Asignatura.deployed();
        // console.log("Asignatura =", asignatura.address);
    });



    it("Matricular a dos alumnos correctamente",  async () => {

        let evaAccount = accounts[1];
        let pepeAccount = accounts[2];

        await asignatura.automatricula("Eva Gomez", "eva_gomez_00@gmail.com", {from: evaAccount});
        await asignatura.automatricula("Jose Ortega", "josore_99@gmail.com", {from: pepeAccount});

        let numMatriculas = await asignatura.matriculasLength();
        assert.equal(numMatriculas, 2, "Tiene que haber dos alumnos matriculados.");

        let direccion0 = await asignatura.matriculas(0);
        let direccion1 = await asignatura.matriculas(1);

        assert.equal(direccion0, evaAccount, "La direccion del primer alumno matriculado esta mal.");
        assert.equal(direccion1, pepeAccount, "La direccion del segundo alumno matriculado esta mal.");

        let matricula0 = await asignatura.datosAlumno(direccion0);
        let matricula1 = await asignatura.datosAlumno(direccion1);

        assert.equal(matricula0.nombre, "Eva Gomez", "El nombre del primer alumno matriculado esta mal.");
        assert.equal(matricula0.email, "eva_gomez_00@gmail.com", "El email del primer alumno matriculado esta mal.");
     
        assert.equal(matricula1.nombre, "Jose Ortega", "El nombre del segundo alumno matriculado esta mal.");
        assert.equal(matricula1.email, "josore_99@gmail.com", "El email del segundo alumno matriculado esta mal.");
    })

    it("No se puede poner la nota de una evaluación inexistente",  async () => {

        let alumno0 = await asignatura.matriculas(1);

        let indice = 3;
        let tipoNota = 1;
        let calificacion = 65;
        await asignatura.califica(alumno0, indice, tipoNota, calificacion); 

        // Primer parcial de Eva
        let nota = await asignatura.calificaciones(alumno0, 15);
        assert.equal(nota.tipo, 1, "La evaluacion no existe.");
        assert.equal(nota.calificacion, 65, "La evaluacion no existe (2).");
    })

});