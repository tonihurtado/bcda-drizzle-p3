import React from 'react';

import {newContextComponents} from "drizzle-react-components";

const {ContractForm} = newContextComponents;

export default (props) => {

    const {drizzle, drizzleState} = props;

    return (
            <ContractForm
                drizzle={drizzle}
                drizzleState={drizzleState}
                contract={"Asignatura"}
                method={"creaEvaluacion"}
                labels={['Nombre', 'Fecha', 'Puntos']}
            />
    );
}