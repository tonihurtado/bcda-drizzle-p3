import React from 'react';

import {newContextComponents} from "drizzle-react-components";

const {AccountData} = newContextComponents;

export default (props) => {

    const {drizzle, drizzleState} = props;

    return (
        <AccountData
            drizzle={drizzle}
            drizzleState={drizzleState}
            accountIndex="0"
            units="ether"
            precision="3"
            render={({address, balance, units}) => (
                <div>
                    <div>Mi direccion: <span style={{color: "red"}}>{address}</span></div>
                    <div>Mi Balance: <span style={{color: "red"}}>{balance}</span> {units}</div>
                </div>
            )}
        />
    );
}