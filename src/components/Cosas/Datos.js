import React from 'react';

import {newContextComponents} from "drizzle-react-components";

const {ContractData} = newContextComponents;

export default (props) => {

    const {drizzle, drizzleState} = props;

    return (
        <ContractData 
        drizzle={drizzle}
        drizzleState={drizzleState}
        contract={"Asignatura"}
        method={"quienSoy"}
        render={({_nombre, _email}) => (
            <div>
                <div>Nombre: <span style={{color: "blue"}}>{_nombre}</span></div>
                <div>Email: <span style={{color: "blue"}}>{_email}</span></div>
            </div>
        )}
    />
    );
}