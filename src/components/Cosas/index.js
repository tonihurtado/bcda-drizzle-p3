import React from 'react';

import {newContextComponents} from "drizzle-react-components";
import Datos from "./Datos";
import Account from "./Account";

const {ContractData} = newContextComponents;

class Cosas extends React.Component {

    state = {
        ready: false,
        evaluacionesLengthKey: null,
        profesor: null
    }

    componentDidMount() {
        this.setState({ready: true});
    }

    componentDidUpdate(prevProps, prevState, snapshoot) {
        const {drizzle, drizzleState} = this.props;

        const instanceState = drizzleState.contracts.Asignatura;
        if (!instanceState || !instanceState.initialized) return;

        if (!this.state.profesor){
            const instance = drizzle.contracts.Asignatura;
            this.setState({
                profesor: instance.methods.profesor.cacheCall()
            });
        }
        if (!this.state.evaluacionesLengthKey) {
            const instance = drizzle.contracts.Asignatura;
            this.setState({
                evaluacionesLengthKey: instance.methods.evaluacionesLength.cacheCall()
            });
        }
    }

    render() {
        const {drizzle, drizzleState} = this.props;

        const instanceState = drizzleState.contracts.Asignatura;
        if (!this.state.ready || !instanceState || !instanceState.initialized) {
            return <span>Initializing...</span>;
        }

        let el = instanceState.evaluacionesLength[this.state.evaluacionesLengthKey];
        el = el ? el.value : 0;

        let ml = instanceState.matriculasLength[this.state.matriculasLengthKey];
        let profesorAddr = instanceState.profesor['0x0']
        let myAddr = drizzleState.accounts[0]

        profesorAddr = profesorAddr ? profesorAddr.value : ''

        let profesorComp = <div><div>Eres el profesor: <span style={{color: "red"}}>{profesorAddr}</span></div></div>

        let accountComp = profesorAddr != myAddr ? <Account drizzle={drizzle} drizzleState={drizzleState}/> : profesorComp
        let datosComp = profesorAddr != myAddr ?<div><h3>Tus datos:</h3> <Datos drizzle={drizzle} drizzleState={drizzleState}/> </div>: <div></div>
        let tbody = [];
        for (let i = 0; i < el; i++) {
            tbody[i] = (
                <ContractData
                    key={"Eva_"+i}
                    drizzle={drizzle}
                    drizzleState={drizzleState}
                    contract={"Asignatura"}
                    method={"evaluaciones"}
                    methodArgs={[i]}
                    render={evaluacion => (
                        <tr key={"EVA-"+i}>
                            <th>E<sub>{i}</sub></th>
                            <td>{evaluacion.nombre}</td>
                            <td>{evaluacion.fecha ? (new Date(1000 * evaluacion.fecha)).toLocaleString() : ""}</td>
                            <td>{(evaluacion.puntos / 10).toFixed(1)}</td>
                            <ContractData
                            drizzle={drizzle}
                            drizzleState={drizzleState}
                            contract={"Asignatura"}
                            method={"calificaciones"}
                            methodArgs={[myAddr,i]}
                            render={nota => (
                                <td key={"p2" + i}>
                                    {nota.tipo === "0" ? "N.P." : ""}
                                    {nota.tipo === "1" ? (nota.calificacion / 10).toFixed(1) : ""}
                                    {nota.tipo === "2" ? (nota.calificacion / 10).toFixed(1) + "(M.H.)" : ""}
                                </td>
                            )}
                        />
                        </tr>
                    )}
                />);
        }

        return (
            <section>
                <h2>Tu cuenta:</h2>
                <h3>{accountComp}</h3>
                <h3>{datosComp}</h3>
                <table border={1}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Puntos</th>
                        <th>Nota</th>
                    </tr>
                    </thead>
                    <tbody>{tbody}</tbody>
                </table>
                
            </section>
            
        );
    }
}

export default Cosas;


