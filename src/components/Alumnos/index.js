import React from 'react';

import AlumnosHead from "./AlumnosHead";
import AlumnosBody from "./AlumnosBody";
import Automatricula from "./Automatricula";

class Alumnos extends React.Component {

    state = {
        ready: false,
        matriculasLengthKey: null
    }

    componentDidMount() {
        this.setState({ready: true});
    }

    componentDidUpdate(prevProps, prevState, snapshoot) {
        const {drizzle, drizzleState} = this.props;

        const instanceState = drizzleState.contracts.Asignatura;
        if (!instanceState || !instanceState.initialized) return;

        const instance = drizzle.contracts.Asignatura;

        let changed = false;

        // Copiar el estado
        let {
            matriculasLengthKey
        } = JSON.parse(JSON.stringify(this.state));

        if (!matriculasLengthKey) {
            matriculasLengthKey = instance.methods.matriculasLength.cacheCall();
            changed = true;
        }

        if (changed) {
            this.setState({
                matriculasLengthKey
            });
        }
    }


    render() {
        const {drizzle, drizzleState} = this.props;

        const instanceState = drizzleState.contracts.Asignatura;
        if (!this.state.ready || !instanceState || !instanceState.initialized) {
            return <span>Initializing...</span>;
        }

        let ml = instanceState.matriculasLength[this.state.matriculasLengthKey];
        let profesorAddr = instanceState.profesor['0x0']
        let myAddr = drizzleState.accounts[0]

        profesorAddr = profesorAddr ? profesorAddr.value : ''
        ml = ml ? ml.value : "??";
        
        let automatricula = profesorAddr != myAddr ? <Automatricula drizzle={drizzle} drizzleState={drizzleState}/> : <div>Los profesores no se pueden matricular</div>

        return (
            <section>
                <h1>{profesorAddr}[{ml}]</h1>
                <table>
                    <AlumnosHead/>
                    <AlumnosBody drizzle={drizzle}
                                 drizzleState={drizzleState}
                                 matriculasLength={ml}/>
                </table>

                {automatricula}
            </section>
        );
    }
}

export default Alumnos;