import React from 'react';

import {newContextComponents} from "drizzle-react-components";

const {ContractForm, ContractData} = newContextComponents;

export default (props) => {

    const {drizzle, drizzleState} = props;

    
    return (
        <section>
            <ContractForm
                drizzle={drizzle}
                drizzleState={drizzleState}
                contract={"Asignatura"}
                method={"automatricula"}
                labels={['Name', 'Email']}
            />

            <ContractData 
                drizzle={drizzle}
                drizzleState={drizzleState}
                contract={"Asignatura"}
                method={"profesor"}
            />
        </section>
    );
}

